---
title: "Pädagogisches Konzept"
---


Besonders wichtig ist es mir, dass sich die Kinder wohl und geborgen fühlen, um eine Atmosphäre liebevoller Zuwendung zu schaffen. Ich habe die Fähigkeit gut zu beobachten und zu erkennen, was ein Kind gerade braucht. Jedes Kind ist einzigartig und es macht mir viel Freude einfühlsam seine Sprache zu entschlüsseln und sein Vertrauen zu gewinnen. So bekommt jedes Kind die Sicherheit seine Umgebung unbeschwert zu erforschen. Das Erkunden der Umgebung bewegt sich oft auf geliebten, erprobten Pfaden. Wir beobachten Tiere und andere interessante Dinge z.B. Fahrzeuge jeglicher Art und erobern die vielen Spielplätze der Umgebung. Es wird geschaukelt, balanciert, gerutscht, geklettert, gebuddelt gematscht oder gemeinsam Obst im Garten genascht.Den Kindern vermittle ich Freude am Malen, Basteln, Singen, Musizieren, Bewegen und Vorlesen. Ein großer Karton kann auch schon einmal zum Haus, zum Bällchenbad oder zur Kuschelhöhle werden, in der gemeinsam Bilderbücher angeschaut werden.

Dem Jahresablauf folgend werden Feste gefeiert, Lieder gesungen, Dinge in der Natur gesammelt und auf einem Jahreszeitenteller dekoriert oder als Spiel und Werkmaterial genutzt. Die Kinder und ich habe viel Freude daran die Fenster und Türen, dem Wechsel der Jahreszeiten folgend, zu gestalten.

Gefördert werden Selbstvertrauen durch selbständiges Handeln und soziales Miteinander. Das Tageskind lernt, sich als Teil einer Gruppe zu empfinden mit allen Aspekten des sozialen Lernens. Die spezifische, individuelle Persönlichkeit eines jeden Kindes soll dabei nicht verloren gehen. Das Miteinander in der Gruppe wird gefördert. Mitgebrachtes Essen teilen, sich begrüßen und verabschieden und einander helfen, werden bald selbstverständlich. Eine altersgerechte Wissensvermittlung in allen Lebensbereichen ist fester Bestandteil des Zusammenseins. Es bleibt den Kindern ausreichend Raum für selbständiges Handeln, um auch eigene Spielideen zu entwickeln und zu experimentieren.

Es gibt bei uns einfache und feste Regeln, über deren Einhaltung ich liebevoll aber konsequent wache. (z. B. es wird nur im Hochstuhl gegessen oder es werden keine Straßenschuhe in der Wohnung getragen)

Kommt ein von mir betreutes Kind in den Kindergarten, wird die Verabschiedung als etwas Positives vermittelt, als bevorstehender neuer und schöner Lebensabschnitt.

# Eingewöhnung

Die Dauer der Eingewöhnung richtet sich nach Ihrem Kind. Ihr Kind soll sich langsam an die neue Umgebung und neue Menschen gewöhnen können.

Sie besuchen zusammen mit ihrem Kind die Tagesmutter möglichst täglich für ca. eine Stunde zum Spielen. Anfangs spielen Sie mit. Später halten Sie sich, z.B. mit einem Buch beschäftigt, in Sichtweite auf. Gegen Ende der Eingewöhnung verlassen Sie uns für kurze Zeit. Die Abwesenheiten werden verlängert. Sie sollten aber im Notfall noch erreichbar sein.

Damit sie ihr Kind auf die Betreuung bei mir einstimmen können, habe ich ein Tagesmutterbilderbuch gestaltet, das sie gerne bei mir entleihen können.

Bitte geben Sie Ihrem Kind ein persönliches Wohlfühlpaket, gefüllt mit vertrauten und geliebten Dingen, mit zur Tagesmutter, Dies können Schmusedecke, Kuscheltier, Schnuller, die gewohnte Nuckelflasche oder den Trinkbecher, das Lieblingsspielzeug und vertraut riechende Ersatzkleidung sein.

Die Eingewöhnung ist abgeschlossen, wenn sich das Kind von der Tagesmutter trösten lässt.

# Betreuung

Für Kinder im Alter ab zwei Monaten bis sie in den Kindergarten kommen biete ich die Betreuung an.

Die Betreuungszeiten sind: Montags bis Freitags von 8:00 Uhr bis 15:00 Uhr

Die Betreuung findet in unserer hellen, ebenerdigen und geräumigen Wohnung mit Garten statt. Es steht ein vielfältiges Spielangebot für alle Entwicklungsstufen zur Verfügung. Die Wohnung besitzt eine große Terrasse, die in den Garten führt. Auf der Terrasse lässt es sich
gut mit dem Bobby-Car fahren. Im Garten befindet sich ein Sandkasten, der nur zum Spielen aufgedeckt wird. Die Wohnung und der Garten sind kindgerecht gestaltet. Es steht viel wunderschönes Spielzeug zur Verfügung. Dieses wird im Wechsel an die Bedürfnisse der Kinder angepasst.

Dank meines reichhaltigen Kinderwagenfuhrparks können bis zu vier Kinder zu Ausflügen im Kinderwagen mitgenommen werden. Bei Bedarf sind fünf Kindersitze in unserem VW-Bus vorhanden.

# Tagesablauf

• Ankunft und Begrüßung
• Gemeinsames Frühstück
• Singen, spielen, backen, malen oder basteln
• Snack oder Picknick
• Freispiel/Spaziergang/Spielplatz/Garten/Bauernhof/Erdbeerfeld  (möglichst oft an der frischen Luft)
• Mittagsschlaf
• Mittagsessen
• Verabschiedung und Abholung

Der Tagesablauf ist an den Bedürfnissen der Kinder orientiert. Alle Kinder wissen genau, was sie erwartet. Dies gibt Sicherheit und hilft, sich zu Recht zu finden.

Spielerisch führe ich die Kinder an das regelmäßige Händewaschen und Zähneputzen nach jeder Mahlzeit heran (Zahnputz-Lied). Die Kinder werden nach Bedarf jeweils unverzüglich gewickelt.

Um den Kindern das Spielen und Toben in einer größeren Gruppe zu ermöglichen und damit auch bei schlechtem Wetter Bewegung im großen Raum möglich ist, besuche ich mit den Kindern jeden Dienstag um 10:00 Uhr einen Spielkreis im evangelischen Gemeindehaus an der Leimenkaut. Dies ist auch eine gute Vorbereitung auf den Kindergarten.

# Mahlzeiten

Das Essen wird von mir täglich frisch und kindergerecht zubereitet. Besonders wichtig für die Kinder ist mir eine glutamatfreie, gesunde und ausgewogene Ernährung. Bis auf Ausnahmen (Geburtstag oder Festtage) gibt es während der Betreuung keine Süßigkeiten. Auf Wunsch können Sie ihrem Kind individuelle Nahrung mitgeben. Die Mahlzeiten werden gemeinsam in den Hochstühlen um den Esstisch eingenommen. Die Hochstühle können für die Kleinsten auch in Ruhestellung gebracht werden. Babys liegen in den Wippen oder auf der Krabbeldecke dabei. Die Essenspauschale beinhaltet ein Frühstück, eine Obstrunde/Snack und ein Mittagessen.

# Schlafen

Für die Tageskinder stehen fünf Betten in einem gesonderten Raum zur Verfügung. Der Raum lässt sich abdunkeln und ist ruhig. Da die Wohnung ebenerdig liegt, besteht auch die Möglichkeit, wenn die Kinder auf einem unserer Ausflüge eingeschlafen sind, einfach im Kinderwagen in die Wohnung zu schieben.

# Kommunikation

Das sogenannte „Tür- und Angelgespräch“ beim Bringen und Abholen Ihres Kindes ist sehr wichtig.
So können Dinge besprochen werden, die das Kind aktuell betreffen z. B. Wie hat das Kind geschlafen? Hat es gut gegessen? Hat es etwas besonders Schönes oder Trauriges erlebt? Zum Wohl des Kindes findet täglich ein Austausch zwischen Eltern und Tagesmutter statt.
Urlaub

Die Tagesmutter hat 30 Tage Urlaub im Jahr. Die genauen Urlaubstermine werden von mir frühzeitig bekannt gegeben. Die Eltern werden ebenfalls gebeten, ihren Urlaub rechtzeitig anzukündigen. Es findet keine Betreuung zwischen Weihnachten und Neujahr statt.

# Krankheitsfall

Bei Krankheit der Tagesmutter oder ihrer Familie versucht die Tagesmutter, bei der Suche nach einer Ersatztagesmutter behilflich zu sein. Um diese Vertretungssituation für ihr Kind zu vereinfachen, bin ich mit benachbarten Tagesmüttern eng vernetzt (regelmäßige Treffen mit anderen Tagesmüttern und deren Tageskindern). Um Ansteckung der anderen Kinder zu verhindern, ist die Betreuung eines ansteckend kranken Kindes leider nicht möglich.
