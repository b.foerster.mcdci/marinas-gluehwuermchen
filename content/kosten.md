---
title: "Kosten"
---

Die Kosten werden hauptsächlich vom Landkreis getragen. Zusätzlich entstehen Betreuungs- und Essenskosten in Höhe von etwa 200 Euro.
