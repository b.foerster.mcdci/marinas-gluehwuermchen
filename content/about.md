---
title: "Über Mich"
---

# Über Mich

Ich bin Marina May. Seit 2022 bin ich ausgebildete Tagespflegerin. Zudem bin ich Kursleiterin in der "Zwergensprache" und selbst Mutter von einem Schul- und einem Kleinkind.
